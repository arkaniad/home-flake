{
  lib,
  osConfig,
  ...
}: {
  imports = [
    ./cli
    ./editors
  ];

  options.isGraphical = lib.mkOption {
    default = osConfig.isGraphical;
    description = "Whether the system is a graphical target";
    type = lib.types.bool;
  };
}
