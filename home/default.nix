{
  config,
  lib,
  pkgs,
  ...
}: let
  inherit (pkgs.stdenv.hostPlatform) isDarwin;
in {
  imports = [./apps.nix];

  home = {
    packages = with pkgs; [
      git-lfs
      git-crypt
      nixfmt-rfc-style
      just
      nvd
      xcp
      nix-output-monitor
      vscode
    ];

    sessionVariables = lib.mkIf isDarwin {
      SSH_AUTH_SOCK = "${config.programs.gpg.homedir}/S.gpg-agent.ssh";
    };
    stateVersion = "24.05";
  };

  programs = {
    home-manager.enable = true;
    man.enable = true;
    # vscode = {
    #   enable = true;
    #   extensions = with pkgs.vscode-extensions; [
    #     dracula-theme.theme-dracula
    #     vscodevim.vim
    #     yzhang.markdown-all-in-one
    #   ];
    # };
  };
}
