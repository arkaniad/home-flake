{
  config,
  lib,
  pkgs,
  flakePath,
  ...
}: let
  inherit (pkgs.stdenv.hostPlatform) isDarwin isLinux;

  settingsJSON =
    config.lib.file.mkOutOfStoreSymlink
    "${flakePath}/configs/vscode/settings.json";
in {
  programs.vscode = {
    enable = config.isGraphical;
    extensions =
      (with pkgs.vscode-extensions; [
        # patches
        ms-vscode-remote.remote-ssh

        # needs a pinned release
        github.vscode-pull-request-github
      ])
      # pinned releases; these install the latest rather than the nightly version
      ++ (with pkgs.vscode-marketplace-release; [
        eamodio.gitlens
        rust-lang.rust-analyzer
        vadimcn.vscode-lldb
      ])
      ++ (with pkgs.vscode-marketplace; [
        # some default config patching to make these work without needing devShells all the time.
        # other extensions like Go/Rust are only really used with devShells,
        # nix & shell are universal enough for me to want them everywhere.
        (jnoortheen.nix-ide.overrideAttrs (prev: {
          nativeBuildInputs =
            prev.nativeBuildInputs
            ++ [pkgs.jq pkgs.moreutils];
          postInstall = ''
            cd "$out/$installPrefix"
            jq -e '
              .contributes.configuration.properties."nix.formatterPath".default =
                "${pkgs.alejandra}/bin/alejandra" |
              .contributes.configuration.properties."nix.enableLanguageServer".default =
                "true" |
              .contributes.configuration.properties."nix.serverPath".default =
                "${pkgs.nil}/bin/nil" |
              .contributes.configuration.properties."nix.serverSettings".default.nil.formatting.command[0] =
                "${pkgs.alejandra}/bin/alejandra"
            ' < package.json | sponge package.json
          '';
        }))
        (mads-hartmann.bash-ide-vscode.overrideAttrs (prev: {
          nativeBuildInputs =
            prev.nativeBuildInputs
            ++ [pkgs.jq pkgs.moreutils];
          postInstall = ''
            cd "$out/$installPrefix"
            jq -e '
              .contributes.configuration.properties."bashIde.shellcheckPath".default =
                "${pkgs.shellcheck}/bin/shellcheck"
            ' < package.json | sponge package.json
          '';
        }))
        (mkhl.shfmt.overrideAttrs (prev: {
          nativeBuildInputs =
            prev.nativeBuildInputs
            ++ [pkgs.jq pkgs.moreutils];
          postInstall = ''
            cd "$out/$installPrefix"
            jq -e '
              .contributes.configuration.properties."shfmt.executablePath".default =
                "${pkgs.shfmt}/bin/shfmt"
            ' < package.json | sponge package.json
          '';
        }))
        1
        yib.rust-bundle
        4
        ops.packer
        4
        ops.terraform
        ahmadalli.vscode-nginx-conf
        arrterian.nix-env-selector
        bbenoist.nix
        bbenoist.vagrant
        brettm12345.nixfmt-vscode
        castwide.ruby-debug
        castwide.solargraph
        compilenix.vscode-zonefile
        connorshea.vscode-ruby-test-adapter
        davidanson.vscode-markdownlint
        dhoeric.ansible-vault
        doggy8088.k8s-snippets
        donjayamanne.githistory
        donjayamanne.python-environment-manager
        donjayamanne.python-extension-pack
        dustypomerleau.rust-syntax
        eamodio.gitlens
        ecmel.vscode-html-css
        eriklynd.json-tools
        formulahendry.docker-explorer
        formulahendry.docker-extension-pack
        formulahendry.vscode-mysql
        foxundermoon.shell-format
        fredwangwang.vscode-hcl-format
        github.vscode-github-actions
        github.vscode-pull-request-github
        gitlab.gitlab-workflow
        golang.go
        grafana.vscode-jsonnet
        groksrc.ruby
        hangxingliu.vscode-nginx-conf-hint
        hashicorp.hcl
        hashicorp.terraform
        hbenl.vscode-test-explorer
        hediet.vscode-drawio
        heptio.jsonnet
        hoovercj.ruby-linter
        ibm.output-colorizer
        iliazeus.vscode-ansi
        ipedrazas.kubernetes-snippets
        jakebathman.mysql-syntax
        jebbs.plantuml
        jnbt.vscode-rufo
        jscearcy.rust-doc-viewer
        jtavin.ldif
        kevinrose.vsc-python-indent
        korekontrol.saltstack
        lizebang.bash-extension-pack
        marcostazi.vs-code-vagrantfile
        mark-tucker.aws-cli-configure
        mateuszdrewniak.ruby-runner
        mathiasfrohlich.kotlin
        miguel-savignano.ruby-symbols
        milovidov.escape-quotes
        mindaro-dev.file-downloader
        misogi.ruby-rubocop
        mitchdenny.ecdc
        mkhl.direnv
        mohd-akram.vscode-html-format
        ms-kubernetes-tools.vscode-kubernetes-tools
        ms-python.debugpy
        ms-python.isort
        ms-python.python
        ms-python.vscode-pylance
        ms-toolsai.jupyter
        ms-toolsai.jupyter-keymap
        ms-toolsai.vscode-jupyter-cell-tags
        ms-toolsai.vscode-jupyter-slideshow
        ms-vscode-remote.remote-containers
        ms-vscode-remote.remote-ssh
        ms-vscode-remote.remote-ssh-edit
        ms-vscode-remote.remote-wsl
        ms-vscode-remote.vscode-remote-extensionpack
        ms-vscode.cmake-tools
        ms-vscode.cpptools
        ms-vscode.cpptools-extension-pack
        ms-vscode.cpptools-themes
        ms-vscode.makefile-tools
        ms-vscode.powershell
        ms-vscode.remote-explorer
        ms-vscode.remote-server
        ms-vscode.test-adapter-converter
        ms-vscode.vscode-node-azure-pack
        njpwerner.autodocstring
        okteto.remote-kubernetes
        oliversturm.fix-json
        owenfarrell.vscode-vault
        p1c2u.docker-compose
        pinage404.nix-extension-pack
        qezhu.gitlink
        randomchance.logstash
        raynigon.nginx-formatter
        rebornix.ruby
        redhat.ansible
        redhat.vscode-yaml
        remisa.shellman
        rogalmic.bash-debug
        rpinski.shebang-snippets
        samuelcolvin.jinjahtml
        serayuzgur.crates
        shaimendel.kubernetesapply
        shopify.ruby-lsp
        signageos.signageos-vscode-sops
        teabyii.ayu
        technosophos.vscode-make
        timonwong.shellcheck
        twxs.cmake
        vayan.haml
        visualstudioexptteam.intellicode-api-usage-examples
        visualstudioexptteam.vscodeintellicode
        vmware.vscode-spring-boot
        vortizhe.simple-ruby-erb
        walkme.ruby-extension-pack
        wdhongtw.gpg-indicator
        wholroyd.jinja
        william-voyek.vscode-nginx
        wingrunr21.vscode-ruby
        xdebug.php-debug
        xdebug.php-pack
        yesoreyeram.grafana
        zobo.php-intellisense
      ]);
    mutableExtensionsDir = true;
  };

  home.file = lib.mkIf isDarwin {
    "Library/Application Support/Code/User/settings.json".source = settingsJSON;
  };
  xdg.configFile = lib.mkIf isLinux {
    "Code/User/settings.json".source = settingsJSON;
  };
  xdg.mimeApps.defaultApplications."text/plain" = "code.desktop";
}
