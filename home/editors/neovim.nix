{
  config,
  lib,
  pkgs,
  ...
}: {
  programs.neovim = {
    enable = true;
    extraConfig = "
      syntax on

      set expandtab
      set tabstop=4
      set nocompatible ruler laststatus=2 showcmd showmode number
      set incsearch ignorecase smartcase hlsearch

      autocmd FileType sh setlocal ts=2 sts=2 sw=2 expandtab
      autocmd FileType ruby setlocal ts=2 sts=2 sw=2 expandtab
      autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

      set modelines=1

      filetype plugin indent on
    ";
  };

  programs.neovim.plugins = [
    {
      plugin = pkgs.vimPlugins.gundo-vim;
      config = "
        nnoremap <F5> :GundoToggle<CR>
      ";
    }

    {
      plugin = pkgs.vimPlugins.syntastic;
      config = "
        let g:syntastic_always_populate_loc_list = 1
        let g:syntastic_auto_loc_list = 1
        let g:syntastic_check_on_open = 1
        let g:syntastic_check_on_wq = 0
      ";
    }

    pkgs.vimPlugins.vim-dispatch-neovim
    pkgs.vimPlugins.vim-endwise
    pkgs.vimPlugins.vim-airline

    {
      plugin = pkgs.vimPlugins.vim-airline-themes;
      config = ''
        let g:airline_theme = "solarized_flood"
      '';
    }

    {
      plugin = pkgs.vimPlugins.nerdtree;
      config = ''
        autocmd StdinReadPre * let s:std_in=1
        autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif
        map <C-n> :NERDTreeToggle<CR>
      '';
    }

    pkgs.vimPlugins.nerdtree-git-plugin
    pkgs.vimPlugins.vim-go
    pkgs.vimPlugins.vim-shellcheck
    pkgs.vimPlugins.vim-projectionist
    pkgs.vimPlugins.vim-yaml

    {
      plugin = pkgs.vimPlugins.vim-terraform;
      config = "
        let g:terraform_fmt_on_save=1
        let g:terraform_align=1
      ";
    }

    pkgs.vimPlugins.vim-terraform-completion
    pkgs.vimPlugins.python-syntax
    pkgs.vimPlugins.vim-fugitive
    pkgs.vimPlugins.jedi-vim
    pkgs.vimPlugins.rust-vim
    pkgs.vimPlugins.ansible-vim
    pkgs.vimPlugins.vim-dispatch-neovim
  ];
}
