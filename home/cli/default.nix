{...}: {
  imports = [
    ./git.nix
    ./tmux.nix
    ./zsh.nix
    ./packages.nix
  ];
}
