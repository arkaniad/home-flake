{
  config,
  lib,
  pkgs,
  ...
}: let
  my-kubernetes-helm = with pkgs;
    wrapHelm kubernetes-helm {
      plugins = with pkgs.kubernetes-helmPlugins; [
        helm-secrets
        helm-diff
        helm-git
        helm-cm-push
      ];
    };

  my-helmfile = pkgs.helmfile-wrapped.override {
    inherit (my-kubernetes-helm) pluginsDir;
  };

  basePackages = with pkgs; [
    pre-commit
    procps
    ripgrep
    killall
    qemu
    mariadb
    postgresql
    watch
    bat
    vim
    htop
    curl
    tmux
    wget
    jq
    yq-go
    gnupg
    gnumake
    gnused
    gnutar
    hugo
    ipcalc
    pass
    podman
    pwgen
    ranger
    ripgrep
    rsync
    unzip
    nix-prefetch-git
    zsh
    git
    fzf
    direnv
    zsh-syntax-highlighting
    shellcheck
    gcc
    poetry
    trippy
  ];

  opsPackages = with pkgs; [
    fpm
    operator-sdk
    dyff
    my-kubernetes-helm
    my-helmfile
    argocd
    terraform
    terraform-docs
    kubernetes-helm
    kubectl
    kubectx
    kubeconform
    k9s
    krew
    packer
    vault
    packer
    sops
    kind
    kustomize
    kompose
    doctl
    docker
    lima
    dive
    k9s
    stern
    s3cmd
    checkov
  ];

  pythonPackages = with pkgs; [
    (python3.withPackages (ps:
      with ps; [
        pip
        virtualenv
        python-ldap
        transformers
      ]))
  ];

  devPackages = with pkgs; [
    # Rust
    cargo
    cargo-chef
    rustc

    # Node
    nodejs
    yarn

    # Go
    go

    # Ruby
    ruby

    # Misc
    go-task
    reuse
    crane
  ];
in {
  home.packages =
    basePackages
    ++ opsPackages
    ++ pythonPackages
    ++ devPackages;

  home.sessionVariables = {
    EDITOR = "nvim";
  };
}
