{
  config,
  lib,
  pkgs,
  ...
}: {
  programs.zsh = {
    enable = true;
    #enableAutoSuggestions = true;
    enableCompletion = true;

    syntaxHighlighting = {
      enable = true;
    };

    shellAliases = {
      k = "kubectl";
      kc = "kubectx";
      kn = "kubens";
      vim = "nvim";
    };

    oh-my-zsh = {
      enable = true;
      plugins = ["git" "ruby" "bundler" "docker" "kubectl" "rbenv"];
      theme = "robbyrussell";
    };

    initExtra = ''
      # iTerm2 integration
      [ -f ~/.iterm2_shell_integration.zsh ] && source $HOME/.iterm2_shell_integration.zsh

      # Local path
      export PATH="$PATH:$HOME/.local/bin"

      # gpg fix
      export GPG_TTY=$(tty)

      # Direnv hook
      eval "$(direnv hook zsh)"

      # Go path
      go_bin_path="/usr/local/go/bin"
      go_usr_path="$HOME/dev"
      export GOPATH=$go_usr_path
      [ -d "$go_bin_path" ] && export PATH=$PATH:$go_bin_path
      [ -d "$go_usr_path" ] && export PATH=$PATH:$(go env GOPATH)/bin

      # SSH Agent initialization
      if [ -z "$SSH_AUTH_SOCK" ]; then
          if [ ! -S ~/.ssh/ssh_auth_sock ]; then
              eval `ssh-agent`
              ln -sf "$SSH_AUTH_SOCK" ~/.ssh/ssh_auth_sock
          fi

          export SSH_AUTH_SOCK=~/.ssh/ssh_auth_sock
          ssh-add -l > /dev/null || ssh-add
      fi

      # aws-completer
      if hash aws_completer  1>/dev/null 2>&1; then
          complete -C "$(which aws_completer)" aws
      fi

      # Vault completion
      if hash vault 2>/dev/null; then
        complete -o nospace -C "$(which vault)" vault
      fi

      # Other profile scripts
      for file in ~/.profile.d/*;
        do source $file;
      done
    '';
  };
}
