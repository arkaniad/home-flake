{
  config,
  lib,
  pkgs,
  ...
}: {
  programs.git = {
    enable = true;

    userEmail = "rhea@isomemetric.com";
    userName = "Rhea Danzey";

    signing = {
      signByDefault = true;
      key = "8B746F38F7E74C8B";
    };

    extraConfig = {
      init = {
        defaultBranch = "main";
      };

      pull = {
        rebase = false;
      };
    };

    includes = [
      {
        condition = "gitdir:~/dev/src/gitlab.element.io/";
        contents = {
          user = {
            email = "rdanzey@element.io";
            signingKey = "BC69166FB2668680";
          };
          commit = {
            gpgsign = true;
          };
          alias = {
            commit = "commit --signoff";
          };
        };
      }
      {
        condition = "gitdir:~/dev/src/gitlab.matrix.org/";
        contents = {
          user = {
            email = "rdanzey@element.io";
            signingKey = "BC69166FB2668680";
          };
          commit = {
            gpgsign = true;
          };
          alias = {
            commit = "commit --signoff";
          };
        };
      }
      {
        condition = "gitdir:~/dev/src/code.isomemetric.net/";
        contents = {
          user = {
            email = "rhea@isomemetric.com";
            signingKey = "8B746F38F7E74C8B";
          };
          commit = {
            gpgsign = true;
          };
          alias = {
            commit = "commit --signoff";
          };
        };
      }
    ];
  };
}
