{
  description = "My config";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    darwin = {
      url = "github:lnl7/nix-darwin";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    devenv = {
      url = "github:cachix/devenv";
    };

    flake-parts.url = "github:hercules-ci/flake-parts";
    flake-utils.url = "github:numtide/flake-utils";
    pre-commit-hooks = {
      url = "github:cachix/pre-commit-hooks.nix";
      inputs.flake-compat.follows = "";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.nixpkgs-stable.follows = "nixpkgs";
    };
  };

  outputs = {
    flake-parts,
    self,
    ...
  } @ inputs: let
    inherit (import ./hosts/lib.nix {inherit inputs overlays;}) mkSystems;
    overlays = import ./pkgs/overlays.nix {inherit inputs;};
  in
    flake-parts.lib.mkFlake {inherit self inputs;} {
      flake = mkSystems [
        #        {
        #          host = "raze";
        #          system = "aarch64-darwin";
        #          username = "ark";
        #          isGraphical = true;
        #        }
        {
          host = "calamus";
          system = "aarch64-darwin";
          username = "ark";
          isGraphical = true;
        }
        {
          host = "wintermute";
          system = "x86_64-linux";
          username = "ark";
          isGraphical = true;
        }
      ];
      imports = [inputs.pre-commit-hooks.flakeModule];
      perSystem = {
        config,
        self',
        inputs',
        pkgs,
        system,
        ...
      }: {
        _module.args.pkgs = import inputs.nixpkgs {
          inherit system;
          config.allowUnfree = true;
        };

        pre-commit = {
          check.enable = true;
          settings.hooks = {
            alejandra.enable = true;
            shellcheck.enable = true;
          };
        };

        devShells.default = pkgs.mkShell {
          inherit (config.pre-commit.devShell) shellHook;
          buildInputs = with pkgs;
            [
              alejandra
              git-crypt
              nix-output-monitor
              just
              nil
              nvd
            ]
            ++ lib.optionals stdenv.isDarwin
            [inputs'.darwin.packages.darwin-rebuild];
        };

        legacyPackages.homeConfigurations = let
          homeLib = import ./home/lib.nix {
            inherit inputs pkgs username;
            isNixOS = false;
          };
          username = "ark";
        in {
          ${username} = inputs.home-manager.lib.homeManagerConfiguration {
            inherit pkgs;
            inherit (homeLib) extraSpecialArgs modules;
          };
        };

        formatter = pkgs.alejandra;
      };
      systems = ["aarch64-darwin" "aarch64-linux" "x86_64-darwin" "x86_64-linux"];
    };

  nixConfig = {
    extra-substituters = [
      "https://cache.nixos.org"
      "https://nix-community.cachix.org"
      "https://pre-commit-hooks.cachix.org"
    ];

    extra-trusted-public-keys = [
      "cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY="
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
      "pre-commit-hooks.cachix.org-1:Pkk3Panw5AW24TOv6kz3PvLhlH8puAsJTBbOPmBo7Rc="
    ];
  };
}
