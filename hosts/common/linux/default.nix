{pkgs, ...}: {
  # imports = [
  #   ./boot.nix
  #   ./gnome.nix
  #   ./greeter.nix
  #   ./input.nix
  #   ./network.nix
  #   ./sound.nix
  #   ./sway.nix
  # ];

  programs.nix-ld.enable = true;
  programs.zsh.enable = true;
  environment.systemPackages = [pkgs.xdg-utils];

  system.stateVersion = "24.05";

  services.cron = {
    enable = true;
  };

  # # enable yubikey u2f support
  # security.pam.u2f = {
  #   enable = true;
  #   cue = true;
  # };
}
