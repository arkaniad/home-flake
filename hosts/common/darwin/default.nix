{
  lib,
  pkgs,
  ...
}: {
  programs.zsh.enable = true;

  system.stateVersion = 4;

  security.pam.enableSudoTouchIdAuth = true;

  services = {
    # Auto upgrade nix package and the daemon service.
    nix-daemon.enable = true;
  };

  environment.systemPackages = [
    pkgs.devenv
    pkgs.xcbuild
  ];
}
