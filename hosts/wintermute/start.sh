# shellcheck disable=SC2148
exec 2> >(systemd-cat -t win11)
exec 1>&2

# killall -e sway 2>/dev/null
# echo ">>>> Tried to kill sway" >>/tmp/win11.log
# ps aux | rg sway | rg -v rg && exit 1 # if sway is still running, everything breaks anyways

sleep 1
echo ">>>> Sleeping before we continue"

sync && echo 1 >/proc/sys/vm/drop_caches
echo ">>>> Freed memory to speed up VM POST"

cpupower frequency-set -g performance
echo ">>>> Changed CPU governors to performance"

# Unbind VTconsoles
echo 0 >/sys/class/vtconsole/vtcon0/bind
echo ">>>> Unbound vtcon0"
echo 0 >/sys/class/vtconsole/vtcon1/bind
echo ">>>> Unbound vtcon1"

# Unbind the GPU from display driver
virsh nodedev-detach pci_0000_01_00_0
echo ">>>> Unbound GPU"

virsh nodedev-detach pci_0000_01_00_1
echo ">>>> Unbound HDMI audio"

virsh nodedev-detach pci_0000_07_00_0
echo ">>>> Unbound front USB"
