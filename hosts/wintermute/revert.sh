# shellcheck disable=SC2148
set -x
exec 2> >(systemd-cat -t win11)
exec 1>&2

virsh nodedev-reattach pci_0000_07_00_0
echo ">>>> Rebound Front USB devices"

#doas -u ark \
#  env XDG_RUNTIME_DIR=/run/user/$(id -u ark) DBUS_SESSION_ADDRESS=unix:path=/run/user/$(id -u ark)/bus \
#  systemctl restart --user pipewire.socket pipewire-pulse.socket
#echo ">>>> Restarted user pulse"

virsh nodedev-reattach pci_0000_01_00_1
echo ">>>> Rebound HDMI audio"

virsh nodedev-reattach pci_0000_01_00_0
echo ">>>> Rebound GPU"

# Wait 1 second to avoid possible race condition
sleep 1

# Re-bind to virtual consoles
echo 1 >/sys/class/vtconsole/vtcon0/bind
echo ">>>> Rebound vtcon0"
echo 1 >/sys/class/vtconsole/vtcon1/bind
echo ">>>> Rebound vtcon1"

cpupower frequency-set -g schedutil
echo ">>>> Changed CPU governors to schedutil"

echo ">>>> End"
echo ">>>>"
