{
  pkgs,
  lib,
  config,
  ...
}: let
  # RTX 4080 Ti
  gpuIDs = [
    "10de:2704" # graphics
    "10de:22bb" # audio
  ];

  # https://github.com/PassthroughPOST/VFIO-Tools/blob/0bdc0aa462c0acd8db344c44e8692ad3a281449a/libvirt_hooks/qemu
  # TODO: just pkgs.runCommand and sed the shebang away
  qemuHook =
    pkgs.stdenv.mkDerivation
    {
      name = "qemu-hook";
      src = pkgs.fetchFromGitHub {
        owner = "PassthroughPOST";
        repo = "VFIO-Tools";
        rev = "4f6d505f7ef032b552c9a544f95e8586d954fe26";
        sha256 = "sha256-fz2VT9AqnO22S5V9sDxmGotZ1mZNeqhlmyJoG3pdtxE=";
      };

      installPhase = "cp libvirt_hooks/qemu $out";
    };
in {
  imports = [
    ./hardware.nix
  ];

  # Bootloader.
  boot = {
    kernelPackages = pkgs.linuxPackages_latest;
    kernelParams = [
      "quiet"
      "splash"
      "amd_iommu=on"
      "video=DP-1:3840x2160@60"
    ];

    initrd = {
      kernelModules = ["vfio_pci" "vfio" "vfio_iommu_type1" "amdgpu"];
    };

    loader = {
      grub = {
        efiSupport = true;
        efiInstallAsRemovable = true;

        enable = true;
        device = "nodev";
        useOSProber = true;
      };
    };
  };

  powerManagement.enable = false;

  networking.hostName = "wintermute"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "America/Chicago";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_US.UTF-8";
    LC_IDENTIFICATION = "en_US.UTF-8";
    LC_MEASUREMENT = "en_US.UTF-8";
    LC_MONETARY = "en_US.UTF-8";
    LC_NAME = "en_US.UTF-8";
    LC_NUMERIC = "en_US.UTF-8";
    LC_PAPER = "en_US.UTF-8";
    LC_TELEPHONE = "en_US.UTF-8";
    LC_TIME = "en_US.UTF-8";
  };

  services.xrdp = {
    enable = true;
    defaultWindowManager = "gnome-remote-desktop";
    openFirewall = true;
  };
  # Configure X11
  services.xserver = {
    enable = true;

    # Key mapping
    layout = "us";
    xkbVariant = "";

    # Video settings
    videoDrivers = ["amdgpu"];

    # Desktop environment
    displayManager.gdm.enable = true;
    displayManager.gdm.autoSuspend = false;
    desktopManager.gnome.enable = true;
  };

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.ark = {
    isNormalUser = true;
    description = "Rhea Danzey";
    extraGroups = ["networkmanager" "wheel" "docker" "libvirtd"];
    packages = with pkgs; [
      # net
      pkgs.gnome.gnome-remote-desktop
      firefox
      thunderbird
      discord
      element-desktop

      # util
      joplin
      joplin-desktop
      bitwarden
      nextcloud-client
      bitwarden-cli

      # media
      spotify
      vlc

      # graphics
      gimp
      inkscape
      blender

      # music
      ardour
      puredata
      sunvox

      # gaming
      #cabextract
      #lutris-free
      #mangohud
      #wineWowPackages.staging
      #winetricks

      # system
      pciutils
      usbutils
      pinentry
      ncdu
    ];
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  environment.systemPackages = with pkgs; [
    pkgs.python311Packages.python-ldap
    openldap
    neovim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    wget
    git
    curl
    htop
    tmux
    git
    pkgs.devenv
    virt-manager
  ];

  programs = {
    gamemode = {
      enable = true;
      settings = {
        custom = {
          start = "${pkgs.libnotify}/bin/notify-send 'GameMode started'";
          end = "${pkgs.libnotify}/bin/notify-send 'GameMode ended'";
        };
      };
    };
    steam = {
      enable = true;
      package = pkgs.steam.override {
        extraPkgs = p: with p; [protontricks];
      };
    };
  };

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
    pinentryPackage = pkgs.pinentry-gnome3;
  };

  # List services that you want to enable:
  services = {
    openssh.enable = true;
    pcscd.enable = true;
  };

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  system.stateVersion = "24.05"; # Did you read the comment?

  hardware.opengl = {
    enable = true;
    driSupport = true;
    driSupport32Bit = true;
  };

  virtualisation.docker.enable = true;

  virtualisation.libvirtd = {
    enable = true;
    qemu = {
      package = pkgs.qemu_kvm;
      runAsRoot = true;
      swtpm.enable = true;
      ovmf = {
        enable = true;
        packages = [pkgs.OVMFFull.fd];
      };
    };
  };

  systemd.services.libvirtd = {
    path = let
      env = pkgs.buildEnv {
        name = "qemu-hook-env";
        paths = with pkgs; [
          libvirt
          procps
          utillinux
          doas
          config.boot.kernelPackages.cpupower
          zfs
          ripgrep
          killall
        ];
      };
    in [env];

    preStart = ''
      mkdir -p /var/lib/libvirt/hooks
      mkdir -p /var/lib/libvirt/hooks/qemu.d/win11/prepare/begin
      mkdir -p /var/lib/libvirt/hooks/qemu.d/win11/release/end
      mkdir -p /var/lib/libvirt/hooks/qemu.d/win11/started/begin

      ln -sf ${qemuHook} /var/lib/libvirt/hooks/qemu
      ln -sf ${./start.sh} /var/lib/libvirt/hooks/qemu.d/win11/prepare/begin/start.sh
      ln -sf ${./revert.sh} /var/lib/libvirt/hooks/qemu.d/win11/release/end/revert.sh
      ln -sf ${./fifo.sh} /var/lib/libvirt/hooks/qemu.d/win11/started/begin/fifo.sh
    '';
  };
}
