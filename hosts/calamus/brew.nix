{
  config,
  pkgs,
  ...
}: let
  noQuarantine = name: {
    inherit name;
    args.no_quarantine = true;
  };
  skipSha = name: {
    inherit name;
    args.require_sha = false;
  };
in {
  environment.systemPath = [config.homebrew.brewPrefix];
  environment.shells = [pkgs.zsh];
  users.users.ark = {
    shell = pkgs.zsh;
    name = "ark";
    home = "/Users/ark";
  };

  environment.systemPackages = with pkgs; [
    pkgs.zsh
    pkgs.gcc
    pkgs.libgccjit
  ];

  # Create /etc/zshrc that loads the nix-darwin environment.
  programs.zsh.enable = true; # default shell on catalina
  programs.bash.enable = false;

  # Use a custom configuration.nix location.
  # $ darwin-rebuild switch -I darwin-config=$HOME/.config/nixpkgs/darwin/configuration.nix
  # environment.darwinConfig = "$HOME/.config/nixpkgs/darwin/configuration.nix";

  # Auto upgrade nix package and the daemon service.
  services.nix-daemon.enable = true;
  # nix.package = pkgs.nix;

  # programs.fish.enable = true;

  # Used for backwards compatibility, please read the changelog before changing.
  # $ darwin-rebuild changelog
  system.stateVersion = 4;

  homebrew = {
    enable = true;
    caskArgs.require_sha = true;
    onActivation = {
      autoUpdate = true;
      upgrade = true;
      cleanup = "zap";
    };

    global = {
      brewfile = false;
    };

    extraConfig = ''
      cask "firefox", args: { language: "en-US" }
    '';

    casks = [
      "amethyst"
      "bitwarden"
      "chromium"
      "cyberduck"
      "element"
      "firefox"
      "gpg-suite-no-mail"
      "insomnia"
      "iterm2"
      "jitsi-meet"
      "joplin"
      "lens"
      "libreoffice"
      "macfuse"
      "menumeters"
      "miniconda"
      "minishift"
      "multipass"
      "mysqlworkbench"
      "nextcloud"
      "pgadmin4"
      "spotify"
      "visual-studio-code"
      "the-unarchiver"
      "tunnelblick"
      "vagrant"
      "veracrypt"
      "vlc"
      "wireshark-chmodbpf"
      "jitsi-meet"
      "multipass"
      "visual-studio-code"
    ];

    brews = [
      "aws-iam-authenticator"
      "gettext"
      "gnu-tar"
      "openssh"
      "pinentry-mac"
      "rustup-init"
      "xcodegen"
      "ykman"
      "yubico-piv-tool"
      "hopenpgp-tools"
      "yubikey-personalization"
      "rosa-cli"
      "openshift-cli"
      "derailed/popeye/popeye"
      "helm/tap/chart-releaser"
      "int128/kubelogin/kubelogin"
      "kiliankoe/formulae/swift-outdated"
      "kreuzwerker/taps/m1-terraform-provider-helper"
      "localazy/tools/localazy"
      "norwoodj/tap/helm-docs"
      "ubuntu/microk8s/microk8s"
      "docker-credential-helper"
      "dnsmasq"
      "clamav"
      "operator-sdk"
      "glab"
      "gpgme"
      "skopeo"
    ];
  };
}
