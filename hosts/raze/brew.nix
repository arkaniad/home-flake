{
  config,
  pkgs,
  ...
}: let
  noQuarantine = name: {
    inherit name;
    args.no_quarantine = true;
  };
  skipSha = name: {
    inherit name;
    args.require_sha = false;
  };

  homebrewCasksCommon = [
    "amethyst"
    "bitwarden"
    "chromium"
    "cyberduck"
    "element"
    "firefox"
    "gpg-suite"
    "insomnia"
    "iterm2"
    "joplin"
    "lens"
    "libreoffice"
    "macfuse"
    "menumeters"
    "miniconda"
    "mysqlworkbench"
    "nextcloud"
    "pgadmin4"
    "spotify"
    "visual-studio-code"
    "the-unarchiver"
    "tunnelblick"
    "vagrant"
    "veracrypt"
    "vlc"
    "wireshark"
    "yubico-yubikey-manager"
  ];

  homebrewCasksPersonal = [
    "blender"
    "calibre"
    "caprine"
    "discord"
    "foobar2000"
    "freecad"
    "gimp"
    "google-chrome"
    "handbrake"
    "inkscape"
    "krita"
    "musescore"
    "plugdata"
    "signal"
    "soulseek"
    "steam"
    "supercollider"
    "sweet-home3d"
    "telegram"
    "transmission"
    "youtube-downloader"
  ];
in {
  environment.systemPath = [config.homebrew.brewPrefix];
  environment.shells = [pkgs.zsh];
  users.users.ark = {
    shell = pkgs.zsh;
    name = "ark";
    home = "/Users/ark";
  };

  environment.systemPackages = with pkgs; [
    pkgs.zsh
    pkgs.gcc
    pkgs.libgccjit
  ];

  # Create /etc/zshrc that loads the nix-darwin environment.
  programs.zsh.enable = true; # default shell on catalina
  programs.bash.enable = false;

  # Use a custom configuration.nix location.
  # $ darwin-rebuild switch -I darwin-config=$HOME/.config/nixpkgs/darwin/configuration.nix
  # environment.darwinConfig = "$HOME/.config/nixpkgs/darwin/configuration.nix";

  # Auto upgrade nix package and the daemon service.
  services.nix-daemon.enable = true;
  # nix.package = pkgs.nix;

  # programs.fish.enable = true;

  # Used for backwards compatibility, please read the changelog before changing.
  # $ darwin-rebuild changelog
  system.stateVersion = 4;

  homebrew = {
    enable = true;
    caskArgs.require_sha = true;
    onActivation = {
      autoUpdate = true;
      upgrade = true;
      cleanup = "zap";
    };

    global = {
      brewfile = false;
    };

    extraConfig = ''
      cask "firefox", args: { language: "en-US" }
    '';

    casks = homebrewCasksCommon ++ homebrewCasksPersonal;
    brews = [
      "gettext"
    ];
  };
}
